package assignments.assignment3;

class Dosen extends ElemenFasilkom {


    private MataKuliah mataKuliah;

    //constructor
    public Dosen(String nama) {
        super("Dosen",nama);
        this.mataKuliah = null;

    }
    //method untuk mengeset matakuliah yang diajar oleh dosen
    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        if (this.mataKuliah == null){
            if (mataKuliah.getDosen() != null){
                System.out.println(String.format("[DITOLAK] %s sudah memiliki dosen pengajar",mataKuliah.toString()));
            }
            else{
                this.mataKuliah = mataKuliah;
                mataKuliah.addDosen(this);
                System.out.println(String.format("%s mengajar mata kuliah %s",toString(), mataKuliah.toString()));
            }
        }
        else if (this.mataKuliah != null){
            System.out.println(String.format("[DITOLAK] %s sudah mengajar mata kuliah %s", toString(),this.getMatkul()));
        }
    }
    //method untuk drop matakuliah dari dosen, atau dosen berhenti mengajar dari matkul tersebut
    public void dropMataKuliah() {
        if (this.mataKuliah == null){
            System.out.println(String.format("%s sedang tidak mengajar mata kuliah apapun",toString()));
        }
        else{
            System.out.println(String.format("%s berhenti mengajar %s",toString(),this.mataKuliah.toString()));
            this.mataKuliah.dropDosen();
            this.mataKuliah = null;
        }
    }
    //getter
    public MataKuliah getMatkul(){
        return this.mataKuliah;
    }
}