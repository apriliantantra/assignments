package assignments.assignment3;

abstract class ElemenFasilkom {
    

    private String tipe;
    private String nama;
    private int friendship;
    private ElemenFasilkom[] telahMenyapa;

    //contructor
    public ElemenFasilkom(String tipe, String nama){
        this.tipe = tipe;
        this.nama = nama;
        telahMenyapa = new ElemenFasilkom[100];
    }
    //method untuk menyapa
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        for (int i = 0 ; i < telahMenyapa.length ; i++){
            if (telahMenyapa[i] == null){
                telahMenyapa[i] = elemenFasilkom;
                System.out.println(String.format("%s menyapa dengan %s",this.nama, elemenFasilkom.toString()));
                for (int j = 0 ; j < elemenFasilkom.getTelahMenyapa().length; j++){
                    if (elemenFasilkom.getTelahMenyapa()[j] == null){
                        elemenFasilkom.getTelahMenyapa()[j] = this;
                        break;
                    }
                }
                break;
            }
            else if (telahMenyapa[i].toString().equals(elemenFasilkom.toString())){
                System.out.println(String.format("[DITOLAK] %s telah menyapa %s hari ini",this.nama , elemenFasilkom.toString()));
                break;
            }
        }
        //kondisi jika mahasiswa menyapa dosen dan memiliki matkul yang sama dengan dosen tersebut, begitu juga sebaliknya
        if(this.tipe.equals("Mahasiswa")){
            Mahasiswa mhs = (Mahasiswa) this;
            if (elemenFasilkom.getTipe().equals("Dosen")){
                Dosen dsn = (Dosen) elemenFasilkom;
                for (MataKuliah matkul : mhs.getMatkul()){
                    if (matkul != null){
                        if (matkul.equals(dsn.getMatkul())){
                            setFriendship(2);
                            dsn.setFriendship(2);
                            break;
                        }
                    }
                }
            }
        }
        else if(this.tipe.equals("Dosen")){
            Dosen dsn = (Dosen) this;
            if (elemenFasilkom.getTipe().equals("Mahasiswa")){
                Mahasiswa mhs = (Mahasiswa) elemenFasilkom;
                for (MataKuliah matkul : mhs.getMatkul()){
                    if (matkul != null){
                        if (matkul.equals(dsn.getMatkul())){
                            setFriendship(2);
                            mhs.setFriendship(2);
                            break;
                        }
                    }
                }
            }
        }
    } 
    //method untuk menghapus orang yang telah disapa
    public void resetMenyapa() {
        for (int i = 0 ; i < telahMenyapa.length ; i ++){
            if (telahMenyapa[i] != null){
                telahMenyapa[i] = null;
            }
            else{
                break;
            }
        }
    }
    //method untuk membeli makanan
    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        ElemenKantin penjuals = (ElemenKantin) penjual;
        int adaMakanan = 0; 
        for (int i = 0 ; i < penjuals.getDaftarMakanan().length ; i++){
            if (penjuals.getDaftarMakanan()[i] != null){
                if (penjuals.getDaftarMakanan()[i].toString().equals(namaMakanan)){
                    adaMakanan += 1;
                    pembeli.setFriendship(1);
                    penjual.setFriendship(1);
                    System.out.println(String.format("%s berhasil membeli %s seharga %d",pembeli.toString(), namaMakanan, penjuals.getDaftarMakanan()[i].getHarga()));
                    break;
                }
            }
        }
        if(adaMakanan == 0){
            System.out.println(String.format("[DITOLAK] %s tidak menjual %s",penjual.toString(),namaMakanan));
        }

    }
    //method untuk mengeset nilai friendship
    public void setFriendship(int value){
        this.friendship += value;
        if(this.friendship < 0){
            this.friendship = 0;
        }
        else if (this.friendship > 100){
            this.friendship = 100;
        }
    }

    //getter
    public String toString() {
        return this.nama;
    }
    public String getTipe(){
        return this.tipe;
    }
    public int getTotalMenyapa(){
        int counter = 0;
        for (ElemenFasilkom elemen : telahMenyapa){
            if (elemen != null){
                counter++;
            }
        }
        return counter;
    }
    public ElemenFasilkom[] getTelahMenyapa(){
        return this.telahMenyapa;
    }
    public int getFriendship(){
        return this.friendship;
    }
}