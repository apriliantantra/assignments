package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    

    private Makanan[] daftarMakanan;

    //constructor
    public ElemenKantin(String nama) {
        super("Kantin",nama);
        daftarMakanan = new Makanan[10];
    }
    //method untuk setmakanan yang akan dijual elemen kantin
    public void setMakanan(String nama, long harga) {
        //membuat objek makanan yang akan dimasukan ke array daftar makanan
        Makanan makananBaru = new Makanan(nama,harga);
        for (int i = 0 ; i < daftarMakanan.length ; i++){
            if (makananBaru.equals(daftarMakanan[i])){
                System.out.println(String.format("%s [nama_makanan] sudah pernah terdaftar",nama));
                break;
            }
            else if (daftarMakanan[i] == null){
                daftarMakanan[i] = makananBaru;
                System.out.println(String.format("%s telah mendaftarkan makanan %s dengan harga %d",toString(),nama,harga));
                break;
            }
        }
    }
    //getter
    public Makanan[] getDaftarMakanan(){
        return this.daftarMakanan;
    }
}