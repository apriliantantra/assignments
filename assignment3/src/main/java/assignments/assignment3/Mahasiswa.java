package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    MataKuliah[] daftarMataKuliah;
    private long npm;
    private String tanggalLahir;
    private String jurusan;

    //constructor
    public Mahasiswa(String nama, long npm) {
        super("Mahasiswa",nama);
        this.npm = npm;
        daftarMataKuliah = new MataKuliah[10];
    }
    //method untuk menambahkan matkul yang diambil tiap mahasiswa
    public void addMatkul(MataKuliah mataKuliah) {
        for (int i = 0 ; i < daftarMataKuliah.length; i++){
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i].toString().equals(mataKuliah.toString())){
                System.out.println(String.format("[DITOLAK] %s telah diambil sebelumnya",mataKuliah.toString()));
                break;
            }
            else if (mataKuliah.getTotalMhs() == mataKuliah.getKapasitas()){
                System.out.println(String.format("[DITOLAK] %s telah penuh kapasitasnya.",mataKuliah.toString()));
                break;
            }
            else if (daftarMataKuliah[i] == null){
                daftarMataKuliah[i] = mataKuliah;
                mataKuliah.addMahasiswa(this);
                System.out.println(String.format("%s berhasil menambahkan mata kuliah %s", toString(), mataKuliah.toString() ));
                break;
            }
        }
    }
    //method untuk drop matkul yang diambil oleh mahasiswa
    public void dropMatkul(MataKuliah mataKuliah) {
        int gaadaMatkul = 0;
        int index = 0;
        for (int i = 0; i < daftarMataKuliah.length ; i++){
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i].toString().equals(mataKuliah.toString())){
                index = i;
                daftarMataKuliah[i] = null;
                mataKuliah.dropMahasiswa(this);
                System.out.println(String.format("%s berhasil drop mata kuliah %s",toString(),mataKuliah.toString()));
                gaadaMatkul+=1;

                break;
            }
        }
        if (gaadaMatkul == 0){
            System.out.println(String.format("[DITOLAK] %s belum pernah diambil",mataKuliah.toString()));
        }
        else{
            if (index < getTotalMatkul()-1){
                for (int i = index ; i < daftarMataKuliah.length - 1 ; i ++){
                    daftarMataKuliah[i] = daftarMataKuliah[i+1];
                    if (i >= getTotalMatkul()){
                        break;
                    }
                }
            }
        }

    }
    //method untuk mencari tanggal lahir dari mahasiswa
    public String extractTanggalLahir(long npm) {
        String newNPM = String.valueOf(npm);
        this.tanggalLahir = Integer.parseInt(newNPM.substring(4,6)) + "-" + Integer.parseInt(newNPM.substring(6,8)) + "-" + Integer.parseInt(newNPM.substring(8,12));
        return this.tanggalLahir;
    }
    //method untuk mengcari jurusan dari mahasiswa
    public String extractJurusan(long npm) {
        String newNPM = String.valueOf(npm);
        String jurusan = "";
        if (newNPM.substring(2,4).equals("02")){
            jurusan = "Sistem Informasi";
        }
        else if (newNPM.substring(2,4).equals("01")){
            jurusan = "Ilmu Komputer";
        }
        return jurusan;
    }
    //getter
    public long getNpm(){
        return this.npm;
    }
    public int getTotalMatkul(){
        int counter = 0;
        for (MataKuliah matkul : daftarMataKuliah){
            if (matkul != null){
                counter++;
            }
        }
        return counter;
    }
    public MataKuliah[] getMatkul(){
        return this.daftarMataKuliah;
    }
}