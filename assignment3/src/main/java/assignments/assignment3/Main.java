package assignments.assignment3;

import java.util.Scanner;
import java.util.Arrays;

public class Main {


    private ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    private MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private int totalMataKuliah = 0;
    private int totalElemenFasilkom = 0; 

    //menambahkan elemen mahasiswa ke daftar elemen fasilkom
    private void addMahasiswa(String nama, long npm) {
        Mahasiswa newMahasiswa = new Mahasiswa(nama,npm);
        for (int i = 0 ; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i] == null){
                daftarElemenFasilkom[i] = newMahasiswa;
                totalElemenFasilkom++;
                System.out.println(String.format("%s berhasil ditambahkan",nama));
                break;
            }
        }
    }
    //menambahkan elemen dosen ke daftar elemen fasilkom
    private  void addDosen(String nama) {
        Dosen newDosen = new Dosen(nama);
        for (int i = 0 ; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i] == null){
                daftarElemenFasilkom[i] = newDosen;
                totalElemenFasilkom++;
                System.out.println(String.format("%s berhasil ditambahkan",nama));
                break;
            }
        }
    }
    //menambahkan elemen kantin ke daftar elemen fasilkom
    private void addElemenKantin(String nama) {
        ElemenKantin newKantin = new ElemenKantin(nama);
        for (int i = 0 ; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i] == null){
                daftarElemenFasilkom[i] = newKantin;
                totalElemenFasilkom++;
                System.out.println(String.format("%s berhasil ditambahkan",nama));
                break;
            }
        }
    }
    //method untuk menyapa satu sama lain
    private void menyapa(String objek1, String objek2) {
        ElemenFasilkom newObjek1 = null;
        ElemenFasilkom newObjek2 = null;
        if (objek1.equals(objek2)){
            System.out.println(String.format("[DITOLAK] Objek yang sama tidak bisa saling menyapa"));
        }
        else{
            for (int i = 0; i < daftarElemenFasilkom.length; i++){
                if(daftarElemenFasilkom[i]!=null){
                    if (daftarElemenFasilkom[i].toString().equals(objek1)){
                         newObjek1 = daftarElemenFasilkom[i];
                    }
                    else if(daftarElemenFasilkom[i].toString().equals(objek2)){
                         newObjek2 = daftarElemenFasilkom[i];
                    }
                }
            }
            newObjek1.menyapa(newObjek2);
        }
    }
    //method untuk elemen kantin mengeset makanannya
    private void addMakanan(String objek, String namaMakanan, long harga) {
        for(int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i].toString().equals(objek)){
                if(daftarElemenFasilkom[i].getTipe().equals("Kantin")){
                    ElemenKantin kantin = (ElemenKantin) daftarElemenFasilkom[i];
                    kantin.setMakanan(namaMakanan,harga);
                    break;
                }
                else{
                    System.out.println(String.format("[DITOLAK] %s bukan merupakan elemen kantin", objek));
                    break;
                }
            }
        }
    }
    //method untuk membeli makanan
    private void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom newObjek1 = null;
        ElemenFasilkom newObjek2 = null;
        for(int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i]!=null && daftarElemenFasilkom[i].toString().equals(objek1)){
                newObjek1 = daftarElemenFasilkom[i];
            }
            else if(daftarElemenFasilkom[i]!=null && daftarElemenFasilkom[i].toString().equals(objek2)){
                newObjek2 = daftarElemenFasilkom[i];
            }
        }
        if(newObjek2 != null && !(newObjek2.getTipe().equals("Kantin"))){
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        }
        else if (newObjek2 == null){
            if (!newObjek1.getTipe().equals("Kantin")){
                System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
            }
            else{
                System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
            }
        }
        else{
            newObjek1.membeliMakanan(newObjek1, newObjek2, namaMakanan);
        }
    }
    //method untuk membuat matkul yang tersedia
    private void createMatkul(String nama, int kapasitas) {
        MataKuliah newMatkul = new MataKuliah(nama,kapasitas);
        for(int i = 0 ; i < daftarMataKuliah.length; i++){
            if(daftarMataKuliah[i] == null){
                daftarMataKuliah[i] = newMatkul;
                totalMataKuliah++;
                System.out.println(String.format("%s berhasil ditambahkan dengan kapasitas %d",nama,kapasitas));
                break;
            }
        }
    }
    //method untuk menambahkan matkul ke mahasiswa
    private void addMatkul(String objek, String namaMataKuliah) {
        for(int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i]!=null && daftarElemenFasilkom[i].toString().equals(objek)){
                if(daftarElemenFasilkom[i].getTipe().equals("Mahasiswa")){
                    Mahasiswa mhs = (Mahasiswa)daftarElemenFasilkom[i];
                    for (MataKuliah matkul : daftarMataKuliah){
                        if(matkul.toString().equals(namaMataKuliah)){
                            mhs.addMatkul(matkul);
                            break;
                        }
                    }
                    break;
                }
                else{
                    System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
                }
            }
        }
    }
    //method untuk mahasiswa mendrop matkulnya
    private void dropMatkul(String objek, String namaMataKuliah) {
        for(int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i]!=null && daftarElemenFasilkom[i].toString().equals(objek)){
                if(daftarElemenFasilkom[i].getTipe().equals("Mahasiswa")){
                    Mahasiswa mhs = (Mahasiswa)daftarElemenFasilkom[i];
                    for (MataKuliah matkul : daftarMataKuliah){
                        if(matkul.toString().equals(namaMataKuliah)){
                            mhs.dropMatkul(matkul);
                            break;
                        }
                    }
                    break;
                }
                else{
                    System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
                }
            }
        }
    }
    //method dosen untuk mengajar matkul yang dipilih
    private void mengajarMatkul(String objek, String namaMataKuliah) {
        for(int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i]!=null && daftarElemenFasilkom[i].toString().equals(objek)){
                if(daftarElemenFasilkom[i].getTipe().equals("Dosen")){
                    Dosen dosen = (Dosen)daftarElemenFasilkom[i];
                    for (MataKuliah matkul : daftarMataKuliah){
                        if(matkul.toString().equals(namaMataKuliah)){
                            dosen.mengajarMataKuliah(matkul);
                            break;
                        }
                    }
                    break;
                }
                else{
                    System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
                }
            }
        }
    }
    //method untuk dosen berhenti mengajar matkul yang dipilih
    private void berhentiMengajar(String objek) {
        for(int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i]!=null && daftarElemenFasilkom[i].toString().equals(objek)){
                if(daftarElemenFasilkom[i].getTipe().equals("Dosen")){
                    Dosen dosen = (Dosen)daftarElemenFasilkom[i];
                    dosen.dropMataKuliah();
                    break;
                }
                else{
                    System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
                }
            }
        }
    }
    //method untuk melihat ringkasan mahasiswa
    private void ringkasanMahasiswa(String objek) {
        ElemenFasilkom mhs = null;
        Mahasiswa newMhs = null;
        for(int i = 0 ; i < daftarElemenFasilkom.length ; i ++){
            if(daftarElemenFasilkom[i]!=null && daftarElemenFasilkom[i].toString().equals(objek)){
                if(daftarElemenFasilkom[i].getTipe().equals("Mahasiswa")){
                    mhs = daftarElemenFasilkom[i];
                    newMhs = (Mahasiswa)daftarElemenFasilkom[i];
                    System.out.println("Nama: " + newMhs.toString());
                    System.out.println("Tanggal lahir: " + newMhs.extractTanggalLahir(newMhs.getNpm()));
                    System.out.println("Jurusan: " + newMhs.extractJurusan(newMhs.getNpm()));
                    System.out.println("Daftar Mata Kuliah:" );
                    if(newMhs.getTotalMatkul() == 0){
                        System.out.println("Belum ada mata kuliah yang diambil");
                        break;
                    }
                    else{
                        for(int j = 0 ; j < newMhs.getMatkul().length; j++){
                            if(newMhs.getMatkul()[j]!=null){
                                System.out.println(String.format("%d. %s",j+1,newMhs.getMatkul()[j].toString()));
                            }
                        }
                    }
                    break;
                }
                else{
                    System.out.println(String.format("[DITOLAK] %s bukan merupakan seorang mahasiswa",objek));
                }
            }
        }
    }
    //method untuk melihat ringkasan matakuliah
    private void ringkasanMataKuliah(String namaMataKuliah) {
        for(int i = 0 ; i < daftarMataKuliah.length; i++){
            if(daftarMataKuliah[i]!= null && daftarMataKuliah[i].toString().equals(namaMataKuliah)){
                System.out.println("Nama mata kuliah: " + daftarMataKuliah[i].toString());
                System.out.println("Jumlah mahasiswa: " + daftarMataKuliah[i].getTotalMhs());
                System.out.println("Kapasitas: " + daftarMataKuliah[i].getKapasitas());
                System.out.print("Dosen pengajar: ");
                if (daftarMataKuliah[i].getDosen() == null){
                    System.out.println("Belum ada");
                }
                else{
                    System.out.println(daftarMataKuliah[i].getDosen());
                }
                System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
                if(daftarMataKuliah[i].getTotalMhs()==0){
                    System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
                }
                else{
                    for (int j=0; j<daftarMataKuliah[i].getMahasiswa().length;j++){
                        if(daftarMataKuliah[i].getMahasiswa()[j]!=null){
                            System.out.println(String.format("%d. %s",j+1,daftarMataKuliah[i].getMahasiswa()[j].toString()));
                        }
                    }
                }
            }
        }
    }
    //method untuk melakukan perhitungan nilai friendship
    private void nextDay() {
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        for (int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i] != null){
                ElemenFasilkom elemen = daftarElemenFasilkom[i];
                if ((this.totalElemenFasilkom-1)/2 <= elemen.getTotalMenyapa()){
                    elemen.setFriendship(10);
                }
                else{
                    elemen.setFriendship(-5);
                }
                elemen.resetMenyapa();
            }
        }
        friendshipRanking();
    }
    //method untuk melakukan sorting nila friendship
    private void friendshipRanking() {
        ElemenFasilkom[] newElemen = Arrays.copyOf(daftarElemenFasilkom, daftarElemenFasilkom.length);
        for(int i = newElemen.length ; i > 0 ; i--){
            for (int j = 0 ; j < i-1 ; j++){
                if (newElemen[j] != null && newElemen[j+1]!=null){
                    if (newElemen[j].getFriendship() < newElemen[j+1].getFriendship()){
                        ElemenFasilkom temp = newElemen[j+1];
                        newElemen[j+1] = newElemen[j];
                        newElemen[j] = temp;
                    }
                    else if (newElemen[j].getFriendship() == newElemen[j+1].getFriendship() && newElemen[j].toString().compareTo(newElemen[j+1].toString())>0 ){
                        ElemenFasilkom temp = newElemen[j];
                        newElemen[j] = newElemen[j+1];
                        newElemen[j+1] = temp;
                    }
                }
            }
        }
        // for(int i = newElemen.length ; i > 0 ; i--){
        //     for (int j = 0 ; j < i-1 ; j++){
        //         if (newElemen[j] != null && newElemen[j+1]!=null && newElemen[j].getFriendship() == newElemen[j+1].getFriendship()){
        //             if(newElemen[j].toString().compareTo(newElemen[j+1].toString())>0){
        //                 ElemenFasilkom temp = newElemen[j];
        //                 newElemen[j] = newElemen[j+1];
        //                 newElemen[j+1] = temp;
        //             }
        //         }
        //     }
        // }
        
        //ngeprint ranking mahasiswa
        for (int i = 0 ; i < newElemen.length ; i++){
            if (newElemen[i]!=null){
                System.out.println(String.format("%d. %s(%d)",i+1,newElemen[i].toString(),newElemen[i].getFriendship()));
            }
        }

    }
    //method untuk menghentikan program
    private void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();

    }
    public void run() {
        Scanner input = new Scanner(System.in);
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
    public static void main (String[] args){
        Main program = new Main();
        program.run();
    }
}