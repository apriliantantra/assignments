package assignments.assignment3;

class MataKuliah {


    private String nama;
    private int kapasitas;
    private Dosen dosen;
    private Mahasiswa[] daftarMahasiswa;

    //constructor
    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
        this.dosen = null;
    }
    //menambahkan mahasiswa yang mengambil suatu matkul
    public void addMahasiswa(Mahasiswa mahasiswa) {
        for(int i = 0; i < daftarMahasiswa.length; i++){
            if (daftarMahasiswa[i] == null){
                daftarMahasiswa[i] = mahasiswa;
                break;
            }
        }
    }
    //menghapus mahasiswa yang ada pada matkul tersebut
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        int index = 0;
        for(int i = 0; i<daftarMahasiswa.length; i++){
            if (daftarMahasiswa[i].toString().equals(mahasiswa.toString())){
                index = i;
                daftarMahasiswa[i] = null;
                break;
            }
        }
        if (index < getTotalMhs()-1){
            for (int i = index ; i < daftarMahasiswa.length - 1 ; i ++){
                daftarMahasiswa[i] = daftarMahasiswa[i+1];
                if (i >= getTotalMhs()){
                    break;
                }
            }
        }
    }
    //menambahkan dosen pengajar pada matkul tersebut
    public void addDosen(Dosen dosen) {
        if (this.dosen == null){
            this.dosen = dosen;
        }
    }
    //menghapus dosen yang mengajar pada matkul tersebut
    public void dropDosen() {
        this.dosen = null;
    }
    //getter
    public String toString() {
        return this.nama;
    }
    public Dosen getDosen(){
        return this.dosen;
    }
    public int getKapasitas(){
        return this.kapasitas;
    }
    public int getTotalMhs(){
        int counter = 0;
        for (Mahasiswa mhs : daftarMahasiswa){
            if (mhs != null){
                counter +=1;
            }
        }
        return counter;
    }
    public Mahasiswa[] getMahasiswa(){
        return this.daftarMahasiswa;
    }
}