package assignments.assignment3;

class Makanan {


    private String nama;
    private long harga;

    //constructor
    public Makanan(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }
    //getter
    public String toString(){
        return this.nama;
    }
    public long getHarga(){
        return this.harga;
    }
}