package assignments.assignment1;
import java.util.Scanner;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */

    public static boolean validate(long npm) {
        // TODO: validate NPM, return it with boolean
        String str = String.valueOf(npm); 
        if (str.length() != 14){ //jika digit npm kurang dari 14
            return false;
        }
        int awalMasuk = Integer.parseInt(str.substring(0,2)) + 2000; //menghitung tahun masuk
        int umur =  awalMasuk - Integer.parseInt(str.substring(8,12)); //menghitung umur
        if (umur <= 15){ //jika umur kurang dari 15
            return false;
        }
        String potongNpm = str.substring(0,13); //memotong 1 digit paling belakang dari NPM
        String kodeNpm = String.valueOf(rekursi(potongNpm)); //untuk melakukan kalkulasi seperti syarat
        while (kodeNpm.length() > 1){ //jika masih ada 2 digit setelah kalkulasi
            int intNpm = Integer.parseInt(kodeNpm);
            kodeNpm = String.valueOf(rekursi2(intNpm));
        }
        if (!(String.valueOf(npm%10).equals(kodeNpm))){
            return false;
        }
        return true;
        
    }

    public static String extract(long npm) {
        // TODO: Extract information from NPM, return string with given format
        if (validate(npm) == true){
            String str = String.valueOf(npm);
            String tahunMasuk = "20" + str.substring(0,2); //untuk mencetak tahun masuk
            String status = str.substring(2,4); //sebagai kode Jurusan
            String kodeJurusan = "";
            String tanggalLahir = str.substring(4,6) + "-" + str.substring(6,8) + "-" + str.substring(8,12);
            switch(status){ //mencari kode jurusan yang cocok
                case "01": kodeJurusan = "Ilmu Komputer";
                            break;
                case "02": kodeJurusan = "Sistem Informasi";
                            break;
                case "03": kodeJurusan = "Teknologi Informasi";
                            break;
                case "11": kodeJurusan = "Teknik Telekomunikasi";
                            break;
                case "12": kodeJurusan = "Teknik Elektro";
                            break;
                default : return "NPM Tidak Valid!";
            }
            return "Tahun masuk: " + tahunMasuk + "\n" + "Jurusan: " + kodeJurusan + "\n"
            + "Tanggal Lahir: " + tanggalLahir; //jika kondisi terpenuhi akan me return sesuai dengan perintah
        }
        else{
            return "NPM Tidak Valid!";
        }
    }
    public static long rekursi(String npm){
        long indexing = (long) Math.pow(10,npm.length()-1); //digunakan untuk mengambil digit pertama
        long npm2 = Long.parseLong(npm); 
        if (npm.length() == 1){ //jika npm sudah dipotong hingga tersisa digit ke 7 maka langsung ditambahkan
            return npm2;
        }
        else{
            return ((npm2/indexing) * (npm2%10)) + rekursi(npm.substring(1,(npm.length()-1)));
        }
    }
    public static int rekursi2(int npm){ //rekursi sum of digit
        if ( npm == 0 ){
            return 0;
        }
        else{
            return npm%10 + rekursi2(npm/10);
        }
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }
            else{
                System.out.println(extract(npm));
            }
            // TODO: Check validate and extract NPM
        }
        input.close();
    }
}