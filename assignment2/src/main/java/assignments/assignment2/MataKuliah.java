package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        /* TODO: implementasikan kode Anda di sini */
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        daftarMahasiswa = new Mahasiswa[this.kapasitas];
    }

    //method untuk menambah mahasiswa di sebuah matkul
    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0 ; i < daftarMahasiswa.length;i++){
            if (this.daftarMahasiswa[i] == null){
                this.daftarMahasiswa[i] = mahasiswa;

                break;
            }
        }
    }
    //method untuk mendrop mahasiswa
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0 ; i < daftarMahasiswa.length - 1; i ++){
            if (this.daftarMahasiswa[i] == mahasiswa){
                this.daftarMahasiswa[i] = null;
                Mahasiswa temp = this.daftarMahasiswa[i];
                this.daftarMahasiswa[i] = this.daftarMahasiswa[i+1];
                this.daftarMahasiswa[i+1] = temp;
            }
            else if (this.daftarMahasiswa[i+1]!=null){
                Mahasiswa temp = this.daftarMahasiswa[i];
                this.daftarMahasiswa[i] = this.daftarMahasiswa[i+1];
                this.daftarMahasiswa[i+1] = temp;
            }
        }
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }
    public String getKode(){
        return this.kode;
    }
    public String getNamaMatkul(){
        return this.nama;
    }
    public int getSKS(){
        return this.sks;
    }
    public int getKapasitas(){
        return this.kapasitas;
    }
    public int getTotalMahasiswa(){
        int count = 0;
        for (int i = 0 ; i < daftarMahasiswa.length ; i++){
            if (daftarMahasiswa[i] != null){
                count++;
            }
        }
        return count;
    }
    public Mahasiswa[] getDaftarMahasiswa(){
        return this.daftarMahasiswa;
    }
}
