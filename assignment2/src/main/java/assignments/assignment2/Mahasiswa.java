package assignments.assignment2;
import java.util.Arrays;
import java.util.List;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS = new String[19];
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    //contructor
    public Mahasiswa(String nama, long npm){
        /* TODO: implementasikan kode Anda di sini */
        this.nama   = nama;
        this.npm = npm;
    }
    //method untuk add matkul
    public void addMatkul(MataKuliah matakuliah){
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0 ; i < mataKuliah.length; i++){
            if (this.mataKuliah[i] == null){
                this.mataKuliah[i] = matakuliah;
                this.totalSKS += this.mataKuliah[i].getSKS();
                break;
            }
        }
    }
    //method untuk drop matkul
    public void dropMatkul(MataKuliah matakuliah){
        /* TODO: implementasikan kode Anda di sini */
        //for loop untuk mengganti mata kuliah menjadi index pertama setelah diubah menjadi null
        for (int i = 0 ; i < mataKuliah.length -1 ; i++){
            if (this.mataKuliah[i] == matakuliah){
                this.totalSKS -= this.mataKuliah[i].getSKS();
                this.mataKuliah[i] = null;
                MataKuliah temp = this.mataKuliah[i];
                this.mataKuliah[i] = this.mataKuliah[i+1];
                this.mataKuliah[i+1] = temp;
            }
            else if (this.mataKuliah[i+1]!=null){
                MataKuliah temp = this.mataKuliah[i];
                this.mataKuliah[i] = this.mataKuliah[i+1];
                this.mataKuliah[i+1] = temp;
            }
            
        }
    }


    public void cekIRS(){
        /* TODO: implementasikan kode Anda di sini */
        // untuk menambah masalah jika total sks lebih dari 24
        for (int i = 0 ; i < this.mataKuliah.length ; i++){
            if (this.mataKuliah[i] != null){
                if (this.masalahIRS[i] == null){
                    if (this.totalSKS > 24){
                        if (getTotalMasalah() > 0 && getString() > 0){
                                break;
                        }
                        else{
                            this.masalahIRS[i] = "SKS yang Anda ambil lebih dari 24";
                            break;
                        }
                        
                    }
                }
            }
        }
        // untuk menambah masalah jika matkul yang diambil tidak sesuai
        for (int i = 0 ; i < this.mataKuliah.length ; i++){
            if (this.mataKuliah[i] != null){   
                if (this.masalahIRS[i] == null){
                    if (!(this.mataKuliah[i].getKode().equals(getJurusanS())) && !(this.mataKuliah[i].getKode().equals("CS"))){
                        this.masalahIRS[i] = "Mata Kuliah " + this.mataKuliah[i].getNamaMatkul() + " tidak dapat diambil jurusan " + getJurusanS();
                        break;
                    }
                }
            }
        }
        //mengubah masalah SKS agar menjadi nomor 1 jika ada
        if (getTotalMasalah() > 0 && getString() > 0){
            for (int i = 0; i < getTotalMasalah() ; i++){
                for (int j = getTotalMasalah()-1 ; j > i ; j--){
                    if (!(masalahIRS[0].equals("SKS yang Anda ambil lebih dari 24"))){
                            String temp = masalahIRS[j];
                            masalahIRS[j] = masalahIRS[j-1];
                            masalahIRS[j-1] = temp;
                    }
                }
            }
        }
    }
    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }
    public String getNamaMhs(){
        return this.nama;
    }
    public String getJurusan(){
        String strNPM = String.valueOf(this.npm);
        String status = strNPM.substring(2,4); //sebagai kode Jurusan
        String kodeJurusan = "";
        switch(status){ //mencari kode jurusan yang cocok
            case "01": kodeJurusan = "Ilmu Komputer";
                        break;
            case "02": kodeJurusan = "Sistem Informasi";
                        break;
        }
        return kodeJurusan;
    }
    public long getNpm(){
        return this.npm;
    }
    public int getTotalSKS(){
        return this.totalSKS;
    }
    public MataKuliah[] getMatkul(){
        return this.mataKuliah;
    }
    public String[] getMasalahIRS(){
        return this.masalahIRS;
    }
    //mendapat singkatan dari jurusan
    public String getJurusanS(){
        String jurusan = "";
        if (getJurusan().equals("Ilmu Komputer")){
            jurusan = "IK";
            return jurusan;
        }
        else if (getJurusan().equals("Sistem Informasi")){
            jurusan = "SI";
            return jurusan;
        }
        return null;
    }
    //menghitung jumlah masalah
    public int getTotalMasalah(){
        int counter = 0;
        for (int i = 0 ; i < masalahIRS.length ; i++){
            if (masalahIRS[i] != null){
                counter++;
            }
            else{break;}
        }
        return counter;
    }
    //method untuk mencari apakah masalah tentang SKS sudah ada
    public int getString(){
        int counter = 0;
        for (int i = 0 ; i < masalahIRS.length ; i++){
            if (masalahIRS[i] == null){
                break;
            }
            else if (masalahIRS[i].equals("SKS yang Anda ambil lebih dari 24")){
                counter++;
                break;
            }
        }
        return counter;
    }
    //method untuk mendapatkan total matkul yang diambil mahasiswa
    public int getTotalMatkul(){
        int counter = 0;
        for (int i = 0 ; i < this.mataKuliah.length ; i++){
            if (this.mataKuliah[i] != null){
                counter++;
            }
        }
        return counter;
    }
}
