package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        JPanel newPanel = new JPanel(new GridLayout(5,1,5,5));
        JLabel titleRingkasan = new JLabel("Ringkasan Mata Kuliah", JLabel.CENTER);
        JLabel titlePilihMatkul = new JLabel("Pilih Nama Matkul", JLabel.CENTER);
        JComboBox jcMatkul = new JComboBox();
        JButton lihat = new JButton("Lihat");
        JButton kembali = new JButton("Kembali");
        
        //mengeset warna button dan font dari title
        newPanel.setBorder(new EmptyBorder(50,20,50,20));
        titleRingkasan.setFont(SistemAkademikGUI.fontTitle);
        titlePilihMatkul.setFont(SistemAkademikGUI.newFont);
        lihat.setBackground(new Color(179,252,7));
        lihat.setForeground(Color.WHITE);
        kembali.setBackground(new Color(117,176,242));
        kembali.setForeground(Color.WHITE);

         //sorting
         for(int i = daftarMataKuliah.size() ; i > 0 ; i--){
            for (int j = 0 ; j < i-1 ; j++){
                if (daftarMataKuliah.get(j).getNama().compareTo(daftarMataKuliah.get(j+1).getNama())>0){
                    MataKuliah temp = daftarMataKuliah.get(j);
                    daftarMataKuliah.set(j,daftarMataKuliah.get(j+1));
                    daftarMataKuliah.set(j+1,temp);
                }
            }
        }

        //menambahkan item ke jcbox
        for(MataKuliah elemen : daftarMataKuliah){
            jcMatkul.addItem(elemen.getNama());
        }

        //menambahkan komponen ke panel
        newPanel.add(titleRingkasan);
        newPanel.add(titlePilihMatkul);
        newPanel.add(jcMatkul);
        newPanel.add(lihat);
        newPanel.add(kembali);

        //menambahkan event
        lihat.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    String namaMatkul = String.valueOf(jcMatkul.getSelectedItem());
                    if(namaMatkul.isBlank()){
                        JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    }
                    else{
                        newPanel.setVisible(false);
                        new DetailRingkasanMataKuliahGUI(frame, getMataKuliah(namaMatkul, daftarMataKuliah), daftarMahasiswa, daftarMataKuliah);
                    }
                }
            }
        );
        kembali.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );

        frame.add(newPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
}
