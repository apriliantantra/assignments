package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        JPanel newPanel = new JPanel(new GridLayout(5,1,5,5));
        JLabel titleRingkasan = new JLabel("Ringkasan Mahasiswa", JLabel.CENTER);
        JLabel titlePilihNpm = new JLabel("Pilih NPM", JLabel.CENTER);
        JComboBox jcNPM = new JComboBox();
        JButton lihat = new JButton("Lihat");
        JButton kembali = new JButton("Kembali");

        //mengeset warna button dan font dari title
        newPanel.setBorder(new EmptyBorder(50,20,50,20));
        titleRingkasan.setFont(SistemAkademikGUI.fontTitle);
        titlePilihNpm.setFont(SistemAkademikGUI.newFont);
        lihat.setBackground(new Color(179,252,7));
        lihat.setForeground(Color.WHITE);
        kembali.setBackground(new Color(117,176,242));
        kembali.setForeground(Color.WHITE);

        //sorting
        for(int i = daftarMahasiswa.size() ; i > 0 ; i--){
            for (int j = 0 ; j < i-1 ; j++){
                if (daftarMahasiswa.get(j).getNpm() > daftarMahasiswa.get(j+1).getNpm()){
                    Mahasiswa temp = daftarMahasiswa.get(j+1);
                    daftarMahasiswa.set(j+1,daftarMahasiswa.get(j));
                    daftarMahasiswa.set(j,temp);
                }
            }
        }

         //menambahkan item ke jcbox
         for(Mahasiswa elemen : daftarMahasiswa){
            jcNPM.addItem(elemen.getNpm());
        }

        //menambahkan komponen ke panel
        newPanel.add(titleRingkasan);
        newPanel.add(titlePilihNpm);
        newPanel.add(jcNPM);
        newPanel.add(lihat);
        newPanel.add(kembali);

        //menambahkan event
        lihat.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    long npm = Long.parseLong(String.valueOf(jcNPM.getSelectedItem()));
                    if(String.valueOf(npm).isBlank()){
                        JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    }
                    else{
                        newPanel.setVisible(false);

                        new DetailRingkasanMahasiswaGUI(frame, getMahasiswa(npm, daftarMahasiswa), daftarMahasiswa, daftarMataKuliah);
                    }
                }
            }
        );
        kembali.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );

        frame.add(newPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    
    
    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
