package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI {

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        JPanel newPanel = new JPanel(new GridLayout(7,1,5,5));
        JLabel titleLabel = new JLabel("Tambah IRS", JLabel.CENTER);
        JLabel titlePilihNPM = new JLabel("Pilih NPM", JLabel.CENTER);
        JLabel titlePilihMatkul = new JLabel("Pilih Nama Matkul", JLabel.CENTER);
        JComboBox jcNPM = new JComboBox();
        JComboBox jcMatkul = new JComboBox();
        JButton tambah = new JButton("Tambahkan");
        JButton kembali = new JButton("Kembali");
        
        //mengeset warna button dan font dari title 
        newPanel.setBorder(new EmptyBorder(50,20,50,20));
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titlePilihNPM.setFont(SistemAkademikGUI.newFont);
        titlePilihMatkul.setFont(SistemAkademikGUI.newFont);
        tambah.setBackground(new Color(179,252,7));
        tambah.setForeground(Color.WHITE);
        kembali.setBackground(new Color(117,176,242));
        kembali.setForeground(Color.WHITE);

        //sorting
        for(int i = daftarMahasiswa.size() ; i > 0 ; i--){
            for (int j = 0 ; j < i-1 ; j++){
                if (daftarMahasiswa.get(j).getNpm() > daftarMahasiswa.get(j+1).getNpm()){
                    Mahasiswa temp = daftarMahasiswa.get(j+1);
                    daftarMahasiswa.set(j+1,daftarMahasiswa.get(j));
                    daftarMahasiswa.set(j,temp);
                }
            }
        }
        for(int i = daftarMataKuliah.size() ; i > 0 ; i--){
            for (int j = 0 ; j < i-1 ; j++){
                if (daftarMataKuliah.get(j).getNama().compareTo(daftarMataKuliah.get(j+1).getNama())>0){
                    MataKuliah temp = daftarMataKuliah.get(j);
                    daftarMataKuliah.set(j,daftarMataKuliah.get(j+1));
                    daftarMataKuliah.set(j+1,temp);
                }
            }
        }

        //menambahkan item ke jcbox
        for(Mahasiswa elemen : daftarMahasiswa){
            jcNPM.addItem(elemen.getNpm());
        }
        for(MataKuliah elemen : daftarMataKuliah){
            jcMatkul.addItem(elemen.getNama());
        }

        //menambahkan komponen ke panel
        newPanel.add(titleLabel);
        newPanel.add(titlePilihNPM);
        newPanel.add(jcNPM);
        newPanel.add(titlePilihMatkul);
        newPanel.add(jcMatkul);
        newPanel.add(tambah);
        newPanel.add(kembali);

        //menambahkan event
        tambah.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    int cek = 0;
                    long npm = Long.parseLong(String.valueOf(jcNPM.getSelectedItem()));
                    String namaMatkul = String.valueOf(jcMatkul.getSelectedItem());
                    if(String.valueOf(npm).isBlank() | namaMatkul.isBlank()){
                        JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    }
                    else{
                        for (MataKuliah elemen : getMahasiswa(npm, daftarMahasiswa).getMataKuliah()){
                            if (elemen!=null && elemen.getNama().equals(namaMatkul)){
                                JOptionPane.showMessageDialog(frame, String.format("[DITOLAK] %s telah diambil sebelumnya",namaMatkul));
                                cek +=1;
                                break;
                            }
                        }
                        if (getMataKuliah(namaMatkul,daftarMataKuliah).getKapasitas() == getMataKuliah(namaMatkul,daftarMataKuliah).getJumlahMahasiswa() && cek == 0){
                            JOptionPane.showMessageDialog(frame, String.format("[DITOLAK] %s telah penuh kapasitasnya",namaMatkul));
                        }
                        else if (getMahasiswa(npm, daftarMahasiswa).getBanyakMatkul() >= 10 && cek == 0){
                            JOptionPane.showMessageDialog(frame, "[DITOLAK] Maksimal mata kuliah yang diambil hanya 10");
                        }
                        else if (cek == 0) {
                            getMahasiswa(npm, daftarMahasiswa).addMatkul(getMataKuliah(namaMatkul, daftarMataKuliah));
                            getMahasiswa(npm, daftarMahasiswa).cekIRS();
                            JOptionPane.showMessageDialog(frame, "[BERHASIL] Silakan cek rekap untuk melihat hasil pengecekan IRS");
                        }
                    }
                }
            }
        );
        kembali.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );
        frame.add(newPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }

        private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah){
            for (MataKuliah mataKuliah : daftarMataKuliah) {
                if (mataKuliah.getNama().equals(nama)){
                    return mataKuliah;
                }
            }
            return null;
        }

        private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {
            for (Mahasiswa mahasiswa : daftarMahasiswa) {
                if (mahasiswa.getNpm() == npm){
                    return mahasiswa;
                }
            }
            return null;
        }
}
