package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        JPanel newPanel = new JPanel(new GridLayout(10 + mahasiswa.getBanyakMatkul() + mahasiswa.getBanyakMasalahIRS(),1,1,1));
        JLabel titleDetailRingkasan = new JLabel("Detail Ringkasan Mahasiswa", JLabel.CENTER);
        JLabel titleNama = new JLabel(String.format("Nama: %s",mahasiswa.getNama()), JLabel.CENTER);
        JLabel titleNpm = new JLabel(String.format("NPM: %d",mahasiswa.getNpm()), JLabel.CENTER);
        JLabel titleJurusan = new JLabel(String.format("Jurusan: %s",mahasiswa.getJurusan()), JLabel.CENTER);
        JLabel titleDaftarMatkul = new JLabel("Daftar Mata Kuliah: ", JLabel.CENTER);
        

        //mengeset font
        newPanel.setBorder(new EmptyBorder(70,20,70,20));
        titleDetailRingkasan.setFont(SistemAkademikGUI.fontTitle);
        titleNama.setFont(SistemAkademikGUI.newFont);
        titleNpm.setFont(SistemAkademikGUI.newFont);
        titleJurusan.setFont(SistemAkademikGUI.newFont);
        titleDaftarMatkul.setFont(SistemAkademikGUI.newFont);

        newPanel.add(titleDetailRingkasan);
        newPanel.add(titleNama);
        newPanel.add(titleNpm);
        newPanel.add(titleJurusan);
        newPanel.add(titleDaftarMatkul);
        if (mahasiswa.getBanyakMatkul() == 0){
            JLabel newLabel = new JLabel(("Belum ada mata kuliah yang diambil."),JLabel.CENTER);
            newLabel.setFont(SistemAkademikGUI.fontGeneral);
            newPanel.add(newLabel);
        }
        else{
            for (int i = 0 ; i < mahasiswa.getBanyakMatkul(); i++){
                JLabel newLabel = new JLabel(String.format("%d. %s",i+1,mahasiswa.getMataKuliah()[i].getNama()), JLabel.CENTER);
                newLabel.setFont(SistemAkademikGUI.fontGeneral);
                newPanel.add(newLabel);
            }
        }
        JLabel titleTotalSKS = new JLabel(String.format("Total SKS: %d",mahasiswa.getTotalSKS()), JLabel.CENTER);
        JLabel titleCekIrs = new JLabel("Hasil Pengecekan IRS: ", JLabel.CENTER);
        titleTotalSKS.setFont(SistemAkademikGUI.newFont);
        titleCekIrs.setFont(SistemAkademikGUI.newFont);

        newPanel.add(titleTotalSKS);
        newPanel.add(titleCekIrs);
        if (mahasiswa.getBanyakMasalahIRS() == 0){
            JLabel newLabel = new JLabel("IRS tidak bermasalah.",JLabel.CENTER);
            newLabel.setFont(SistemAkademikGUI.fontGeneral);
            newPanel.add(newLabel);
        }
        
        else{   
            for (int i = 0 ; i < mahasiswa.getBanyakMasalahIRS(); i++){
                JLabel newLabel = new JLabel(String.format("%d. %s",i+1,mahasiswa.getMasalahIRS()[i]),JLabel.CENTER);
                newLabel.setFont(SistemAkademikGUI.fontGeneral);
                newPanel.add(newLabel);
            }
        }
        JButton selesai = new JButton("Selesai");
        selesai.setBackground(new Color(179,252,7));
        selesai.setForeground(Color.WHITE);
        selesai.setFont(SistemAkademikGUI.fontGeneral);
        newPanel.add(selesai);
        selesai.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );
        frame.add(newPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // TODO: Implementasikan Detail Ringkasan Mahasiswa
    }
}
