package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{
    
    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        JPanel newPanel = new JPanel(new GridLayout(7,1,5,5));
        JLabel titleLabel = new JLabel("Tambah Mahasiswa", JLabel.CENTER);
        JLabel titleNama = new JLabel("Nama:",JLabel.CENTER);
        JLabel titleNpm = new JLabel("NPM:",JLabel.CENTER);
        JTextField textNama = new JTextField();
        JTextField textNpm = new JTextField();
        JButton tambah = new JButton("Tambahkan");
        JButton kembali = new JButton("Kembali");
       
        //mengeset warna button dan font dari title
        newPanel.setBorder(new EmptyBorder(70,20,70,20));
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleNama.setFont(SistemAkademikGUI.newFont);
        titleNpm.setFont(SistemAkademikGUI.newFont);
        tambah.setFont(SistemAkademikGUI.fontGeneral);
        kembali.setFont(SistemAkademikGUI.fontGeneral);
        tambah.setBackground(new Color(179,252,7));
        tambah.setForeground(Color.WHITE);
        kembali.setBackground(new Color(117,176,242));
        kembali.setForeground(Color.WHITE);

        //menambahkan kompenan ke panel
        newPanel.add(titleLabel);
        newPanel.add(titleNama);
        newPanel.add(textNama);
        newPanel.add(titleNpm);
        newPanel.add(textNpm);
        newPanel.add(tambah);
        newPanel.add(kembali);
    
        //menambahkan event
        tambah.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    String nama = textNama.getText();
                    String npm =   textNpm.getText();
                    if (nama.isBlank() | npm.isBlank()){
                        JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    }
                    else if (daftarMahasiswa.isEmpty()){
                        Mahasiswa newMhs = new Mahasiswa(nama,Long.parseLong(npm));
                        daftarMahasiswa.add(newMhs);
                        JOptionPane.showMessageDialog(frame, String.format("Mahasiswa %s-%s berhasil ditambahkan",npm,nama));
                        textNama.setText("");
                        textNpm.setText("");
                    }
                    else{
                        Mahasiswa newMhs = new Mahasiswa(nama,Long.parseLong(npm));
                        int sama = 0;
                        for (Mahasiswa elemen : daftarMahasiswa){
                            if (elemen.getNpm() == (newMhs.getNpm())){
                                JOptionPane.showMessageDialog(frame, String.format("NPM %s sudah pernah ditambahkan sebelumnya",npm));
                                textNama.setText("");
                                textNpm.setText("");
                                sama +=1;
                                break;
                            }
                        }
                        if (sama == 0){
                            daftarMahasiswa.add(newMhs);
                            JOptionPane.showMessageDialog(frame, String.format("Mahasiswa %s-%s berhasil ditambahkan",npm,nama));
                            textNama.setText("");
                            textNpm.setText("");
                        }
                    }
                }
            }
        );

        kembali.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );
        frame.add(newPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }   
}
