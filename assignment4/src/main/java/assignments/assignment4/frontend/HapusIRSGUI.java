package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI {

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        JPanel newPanel = new JPanel(new GridLayout(7,1,5,5));
        JLabel sambutan = new JLabel("Hapus IRS", JLabel.CENTER);
        JLabel titlePilihNpm = new JLabel("Pilih NPM", JLabel.CENTER);
        JLabel titlePilihMatkul = new JLabel("Pilih Nama Matkul",JLabel.CENTER);
        JComboBox jcNPM = new JComboBox();
        JComboBox jcMatkul = new JComboBox();
        JButton hapus = new JButton("Hapus");
        JButton kembali = new JButton("Kembali");

        //mengesert warna button dan font dari title
        newPanel.setBorder(new EmptyBorder(50,20,50,20));
        sambutan.setFont(SistemAkademikGUI.fontTitle);
        titlePilihMatkul.setFont(SistemAkademikGUI.newFont);
        titlePilihNpm.setFont(SistemAkademikGUI.newFont);
        hapus.setBackground(new Color(179,252,7));
        hapus.setForeground(Color.WHITE);
        kembali.setBackground(new Color(117,176,242));
        kembali.setForeground(Color.WHITE);

        //sorting
        for(int i = daftarMahasiswa.size() ; i > 0 ; i--){
            for (int j = 0 ; j < i-1 ; j++){
                if (daftarMahasiswa.get(j).getNpm() > daftarMahasiswa.get(j+1).getNpm()){
                    Mahasiswa temp = daftarMahasiswa.get(j+1);
                    daftarMahasiswa.set(j+1,daftarMahasiswa.get(j));
                    daftarMahasiswa.set(j,temp);
                }
            }
        }
        for(int i = daftarMataKuliah.size() ; i > 0 ; i--){
            for (int j = 0 ; j < i-1 ; j++){
                if (daftarMataKuliah.get(j).getNama().compareTo(daftarMataKuliah.get(j+1).getNama())>0){
                    MataKuliah temp = daftarMataKuliah.get(j);
                    daftarMataKuliah.set(j,daftarMataKuliah.get(j+1));
                    daftarMataKuliah.set(j+1,temp);
                }
            }
        }

        //menambahkan item ke jcbox
        for(Mahasiswa elemen : daftarMahasiswa){
            jcNPM.addItem(elemen.getNpm());
        }
        for(MataKuliah elemen : daftarMataKuliah){
            jcMatkul.addItem(elemen.getNama());
        }

        //menambahkan komponen ke panel
        newPanel.add(sambutan);
        newPanel.add(titlePilihNpm);
        newPanel.add(jcNPM);
        newPanel.add(titlePilihMatkul);
        newPanel.add(jcMatkul);
        newPanel.add(hapus);
        newPanel.add(kembali);

        //menambahkan event
        hapus.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    long npm = Long.parseLong(String.valueOf(jcNPM.getSelectedItem()));
                    String namaMatkul = String.valueOf(jcMatkul.getSelectedItem());
                    if(String.valueOf(npm).isBlank() | namaMatkul.isBlank()){
                        JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    }
                    else if (getMahasiswa(npm, daftarMahasiswa).getBanyakMatkul() == 0){
                        JOptionPane.showMessageDialog(frame, "[DITOLAK] Belum ada mata kuliah yang diambil.");
                    }
                    else if (getMahasiswa(npm, daftarMahasiswa).getBanyakMatkul() != 0){
                        for (MataKuliah elemen : getMahasiswa(npm, daftarMahasiswa).getMataKuliah()){
                            if (elemen != null && !elemen.equals(getMataKuliah(namaMatkul,daftarMataKuliah))){
                                JOptionPane.showMessageDialog(frame, String.format("[DITOLAK] %s belum pernah diambil.",namaMatkul));
                            }
                            else if (elemen != null && elemen.equals(getMataKuliah(namaMatkul,daftarMataKuliah))){
                                getMahasiswa(npm, daftarMahasiswa).dropMatkul(getMataKuliah(namaMatkul, daftarMataKuliah));
                                getMahasiswa(npm, daftarMahasiswa).cekIRS();
                                JOptionPane.showMessageDialog(frame,"[BERHASIL] Silakan cek rekap untuk melihat hasil pengecekan IRS.");
                            }
                        }
                    }
                }
            }
        );
        kembali.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );
        frame.add(newPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    
    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMatakuliah) {

        for (MataKuliah mataKuliah : daftarMatakuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
