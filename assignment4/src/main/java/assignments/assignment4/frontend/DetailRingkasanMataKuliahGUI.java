package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        JPanel newPanel = new JPanel(new GridLayout(10 + mataKuliah.getJumlahMahasiswa(),1));
        JLabel titleDetailRingkasan = new JLabel("Detail Ringkasan Mata Kuliah", JLabel.CENTER);
        JLabel titleNama = new JLabel(String.format("Nama: %s",mataKuliah.getNama()), JLabel.CENTER);
        JLabel titleKode = new JLabel(String.format("Kode: %s",mataKuliah.getKode()), JLabel.CENTER);
        JLabel titleSKS = new JLabel(String.format("SKS: %s",mataKuliah.getSKS()), JLabel.CENTER);
        JLabel titleJumlahMhs = new JLabel(String.format("Jumlah mahasiswa: %d",mataKuliah.getJumlahMahasiswa()), JLabel.CENTER);
        JLabel titleKapasitas = new JLabel(String.format("Kapasitas: %d",mataKuliah.getKapasitas()),JLabel.CENTER);
        JLabel titleDaftarMhs = new JLabel ("Daftar Mahasiswa",JLabel.CENTER);
        
        //mengeset font
        newPanel.setBorder(new EmptyBorder(70,20,70,20));
        titleDetailRingkasan.setFont(SistemAkademikGUI.fontTitle);
        titleNama.setFont(SistemAkademikGUI.newFont);
        titleKode.setFont(SistemAkademikGUI.newFont);
        titleSKS.setFont(SistemAkademikGUI.newFont);
        titleJumlahMhs.setFont(SistemAkademikGUI.newFont);
        titleKapasitas.setFont(SistemAkademikGUI.newFont);
        titleDaftarMhs.setFont(SistemAkademikGUI.newFont);

        newPanel.add(titleDetailRingkasan);
        newPanel.add(titleNama);
        newPanel.add(titleKode);
        newPanel.add(titleSKS);
        newPanel.add(titleJumlahMhs);
        newPanel.add(titleKapasitas);
        newPanel.add(titleDaftarMhs);
        if (mataKuliah.getJumlahMahasiswa() == 0){
            newPanel.add(new JLabel("Belum ada mahasiswa yang mengambil mata kuliah ini."),JLabel.CENTER);
        }
        else{
            for (int i = 0 ; i < mataKuliah.getJumlahMahasiswa(); i++){
                JLabel newLabel = new JLabel(String.format("%d. %s",i+1,mataKuliah.getDaftarMahasiswa()[i].getNama()), JLabel.CENTER);
                newLabel.setFont(SistemAkademikGUI.fontGeneral);
                newPanel.add(newLabel);
            }
        }
        JButton selesai = new JButton("Selesai");
        selesai.setBackground(new Color(179,252,7));
        selesai.setForeground(Color.WHITE);
        selesai.setFont(SistemAkademikGUI.fontGeneral);
        newPanel.add(selesai);
        selesai.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );
        frame.add(newPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // TODO: Implementasikan Detail Ringkasan Mata Kuliah
        
    }
}
