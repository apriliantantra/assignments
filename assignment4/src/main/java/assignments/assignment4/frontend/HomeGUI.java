package assignments.assignment4.frontend;

import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        JLabel titleLabel = new JLabel("Selamat datang di Sistem Akademik",JLabel.CENTER);
        JPanel newPanel = new JPanel(new GridLayout(7,1,8,8));
        JButton tambahMhs = new JButton("Tambah Mahasiswa");
        JButton tambahMatkul = new JButton("Tambah Mata Kuliah");
        JButton tambahIRS = new JButton("Tambah IRS");
        JButton hapusIRS = new JButton("Hapus IRS");
        JButton lihatRingkasanMhs = new JButton("Lihat Ringkasan Mahasiswa");
        JButton lihatRingkasanMatkul = new JButton("Lihar Ringkasan Mata Kuliah");

        // mengeset font dari label dan Jpanel
        newPanel.setBorder(new EmptyBorder(10,20,30,20));
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        tambahMhs.setFont(SistemAkademikGUI.fontGeneral);
        tambahMatkul.setFont(SistemAkademikGUI.fontGeneral);
        lihatRingkasanMatkul.setFont(SistemAkademikGUI.fontGeneral);
        lihatRingkasanMhs.setFont(SistemAkademikGUI.fontGeneral);
        hapusIRS.setFont(SistemAkademikGUI.fontGeneral);
        tambahIRS.setFont(SistemAkademikGUI.fontGeneral);

        //mengeset warna background
        tambahMhs.setBackground(new Color(179,252,7));
        tambahMatkul.setBackground(new Color(179,252,7));
        tambahIRS.setBackground(new Color(179,252,7));
        hapusIRS.setBackground(new Color(179,252,7));
        lihatRingkasanMhs.setBackground(new Color(179,252,7));
        lihatRingkasanMatkul.setBackground(new Color(179,252,7));

        //mengeset warna foreground
        tambahMhs.setForeground(Color.WHITE);
        tambahMatkul.setForeground(Color.WHITE);
        tambahIRS.setForeground(Color.WHITE);
        hapusIRS.setForeground(Color.WHITE);
        lihatRingkasanMhs.setForeground(Color.WHITE);
        lihatRingkasanMatkul.setForeground(Color.WHITE);

        //menambahkan button dan label ke panel
        newPanel.add(titleLabel);
        newPanel.add(tambahMhs);
        newPanel.add(tambahMatkul);
        newPanel.add(tambahIRS);
        newPanel.add(hapusIRS);
        newPanel.add(lihatRingkasanMhs);
        newPanel.add(lihatRingkasanMatkul);

        //menambahkan event
        tambahMhs.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );
        tambahMatkul.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );

        tambahIRS.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );

        hapusIRS.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );

        lihatRingkasanMhs.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );

        lihatRingkasanMatkul.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );

        frame.add(newPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
