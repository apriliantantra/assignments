package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        JPanel newPanel = new JPanel(new GridLayout(11,1,5,5));
        JLabel titleLabel = new JLabel("Tambah Mata Kuliah", JLabel.CENTER);
        JLabel titleKodeMatkul = new JLabel("Kode Mata Kuliah:", JLabel.CENTER);
        JLabel titleNamaMatkul = new JLabel("Nama Mata Kuliah:", JLabel.CENTER);
        JLabel titleSks = new JLabel("SKS:", JLabel.CENTER);
        JLabel titleKapasitas = new JLabel("Kapasitas:", JLabel.CENTER);
        JTextField textKodeMatkul = new JTextField();
        JTextField textNamaMatkul = new JTextField();
        JTextField textSks = new JTextField();
        JTextField textKapasitas = new JTextField();
        JButton tambah = new JButton("Tambahkan");
        JButton kembali = new JButton("Kembali");
        
        //mengeset warna button dan font dari title
        newPanel.setBorder(new EmptyBorder(10,20,10,20));
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleKodeMatkul.setFont(SistemAkademikGUI.newFont);
        titleNamaMatkul.setFont(SistemAkademikGUI.newFont);
        titleSks.setFont(SistemAkademikGUI.newFont);
        titleKapasitas.setFont(SistemAkademikGUI.newFont);
        tambah.setBackground(new Color(179,252,7));
        tambah.setForeground(Color.WHITE);
        kembali.setBackground(new Color(117,176,242));
        kembali.setForeground(Color.WHITE);

        //menambahkan kompenan ke panel
        newPanel.add(titleLabel);
        newPanel.add(titleKodeMatkul);
        newPanel.add(textKodeMatkul);
        newPanel.add(titleNamaMatkul);
        newPanel.add(textNamaMatkul);
        newPanel.add(titleSks);
        newPanel.add(textSks);
        newPanel.add(titleKapasitas);
        newPanel.add(textKapasitas);
        newPanel.add(tambah);
        newPanel.add(kembali);
        
        //menambahkan event
        tambah.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    String kodeMatkul = textKodeMatkul.getText();
                    String namaMatkul = textNamaMatkul.getText();
                    String totalSKS = textSks.getText();
                    String kapasitas = textKapasitas.getText();
                    if (kodeMatkul.isBlank() | namaMatkul.isBlank() | totalSKS.isBlank() | kapasitas.isBlank()){
                        JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    }
                    else if (daftarMataKuliah.isEmpty()){
                        MataKuliah newMatkul = new MataKuliah(kodeMatkul, namaMatkul, Integer.parseInt(totalSKS), Integer.parseInt(kapasitas));
                        daftarMataKuliah.add(newMatkul);
                        JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s berhasil ditambahkan",namaMatkul));
                        textKodeMatkul.setText("");
                        textNamaMatkul.setText("");
                        textSks.setText("");
                        textKapasitas.setText("");
                    }
                    else{
                        MataKuliah newMatkul = new MataKuliah(kodeMatkul, namaMatkul, Integer.parseInt(totalSKS), Integer.parseInt(kapasitas));
                        int sama = 0;
                        for (MataKuliah elemen : daftarMataKuliah){
                            if (elemen.equals(newMatkul)){
                                JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya",namaMatkul));
                                textKodeMatkul.setText("");
                                textNamaMatkul.setText("");
                                textSks.setText("");
                                textKapasitas.setText("");
                                sama +=1;
                                break;
                            }
                        }
                        if (sama == 0){
                            daftarMataKuliah.add(newMatkul);
                            JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s berhasil ditambahkan",namaMatkul));
                            textKodeMatkul.setText("");
                            textNamaMatkul.setText("");
                            textSks.setText("");
                            textKapasitas.setText("");
                        }
                    }
                }
            }
        );
        kembali.addActionListener(
            new ActionListener(){
                public void actionPerformed(ActionEvent E){
                    newPanel.setVisible(false);
                    new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                }
            }
        );
        frame.add(newPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
